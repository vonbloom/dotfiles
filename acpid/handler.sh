#!/bin/bash
# Default acpi script that takes an entry for all actions

group=${1%/*}
action=${1#*/}
device=$2
id=$3
value=$4

logger "ACPI event: group[ $group ] action[ $action ] device[ $device ] id[ $id ] value[ $value ]"


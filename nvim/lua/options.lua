local option = vim.opt

option.nu = true
option.relativenumber = true

option.tabstop = 4
option.softtabstop = 4
option.shiftwidth = 4
option.expandtab = true

autoload -U colors && colors
last_status="%(?.%{$fg[green]%}✔.%{$fg[red]%}✖)"
user="%{$fg[yellow]%}%n"
computer="%{$fg[blue]%}%M"
PS1="$last_status"' %B'"$user"'%{$fg[green]%}@'"$computer"' %{$fg[magenta]%}%~$(git_super_status)%{$reset_color%}$%b '
setopt autocd

HISTSIZE=1000000
SAVEHIST=1000000
HISTORY_IGNORE="(ls|cd|pwd|exit|cd ..)"
HISTFILE="$XDG_CACHE_HOME"/zsh/history

export EDITOR=vim
export VISUAL=vim
autoload edit-command-line
zle -N edit-command-line
bindkey '^E' edit-command-line

setopt EXTENDED_HISTORY
setopt HIST_FIND_NO_DUPS
setopt HIST_SAVE_NO_DUPS
setopt HIST_IGNORE_DUPS
setopt HIST_IGNORE_ALL_DUPS
setopt HIST_IGNORE_SPACE
setopt INC_APPEND_HISTORY
setopt MENU_COMPLETE

bindkey -v

zstyle :compinstall filename "$XDG_CONFIG_HOME"/zsh/.zshrc

autoload -Uz compinit
zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}'
zstyle ':completion:*' menu select
zstyle ':completion:*' cache-path $XDG_CACHE_HOME/zsh/zcompcache
zmodload zsh/complist
compinit -d $XDG_CACHE_HOME/zsh/zcompdump-$ZSH_VERSION

source "$XDG_CONFIG_HOME"/shell/aliases

source "$XDG_DATA_HOME"/zsh/plugins/zsh-git-prompt/zshrc.sh
export ZSH_THEME_GIT_PROMPT_PREFIX="%{$fg_bold[magenta]%}("
export ZSH_THEME_GIT_PROMPT_SUFFIX="%{$fg_bold[magenta]%})"
export ZSH_THEME_GIT_PROMPT_BRANCH="%{$fg_bold[white]%}"

source /usr/share/LS_COLORS/dircolors.sh
source /usr/share/zsh/plugins/zsh-history-substring-search/zsh-history-substring-search.zsh
bindkey '^[[A' history-substring-search-up
bindkey '^[[B' history-substring-search-down

# Plugins
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

eval "$(rbenv init -)"

neofetch

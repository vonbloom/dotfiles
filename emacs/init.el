;; Disable start page
(setq inhibit-startup-message 1)
;; Disable audible bell
(setq visible-bell 1)

;; Disable tool bar
(tool-bar-mode -1)
;; Disable menu bar
(menu-bar-mode -1)
;; Disable scroll bar
(scroll-bar-mode -1)
;; Enable line numbers globally
(global-display-line-numbers-mode 1)

;; Set font
(set-face-attribute 'default nil :font "MesloLGM Nerd Font Mono")

;; Initialize package sources
(require 'package)

(setq package-archives '(("melpa" . "https://melpa.org/packages/")
                         ("org" . "https://orgmode.org/elpa/")
                         ("elpa" . "https://elpa.gnu.org/packages/")))

(package-initialize)
(unless package-archive-contents
   (package-refresh-contents))

;; Initialize use-package on non-Linux platforms
(unless (package-installed-p 'use-package)
  (package-install 'use-package))
(require 'use-package)

;; On non-Guix systems, "ensure" packages by default
(setq use-package-always-ensure t)

(use-package doom-themes)
;; Load theme
(load-theme 'doom-nord t)

(use-package doom-modeline
  :ensure t
  :init (doom-modeline-mode 1))

(use-package ivy :config (ivy-mode 1))
(use-package vimrc-mode)
(use-package dockerfile-mode)

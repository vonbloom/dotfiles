SUDO		:= sudo
INSTALLER	:= xbps-install 
CONTROLLER	:= systemctl
AUR_URL		:= https://aur.archlinux.org

INSTALL		:= $(SUDO) $(INSTALLER) -S
ENABLE		:= $(SUDO) $(CONTROLLER) --now enable
LINK		:= ln -fsv
CLONE		:= git clone
MAKEDIR		:= mkdir -p

PWD		:= $(shell pwd)
HOME		:=$(HOME)

help: ## This help dialog
	@awk 'BEGIN {FS = ":.*# "}; /^[a-zA-Z_-]+:.*?## .*$$/ {printf "\033[36m%-24s\033[0m %s\n", $$1, $$2}' \
	$(MAKEFILE_LIST) | sort

acpid: bin dunst ## Install ACPI events manager
	$(INSTALL) $@
	$(SUDO) $(LINK) $(PWD)/$@/handler.sh /etc/acpi/handler.sh
	cd $(PWD)/$@/events && ls | xargs -I {} $(SUDO) $(LINK) $$(readlink -f {}) /etc/acpi/events/{}
	$(SUDO) sed -i 's/#HandleLidSwitch=suspend/HandleLidSwitch=ignore/' /etc/systemd/logind.conf
	$(ENABLE) $@

amdgpu: ## Install open source amd graphic drivers
	$(INSTALL) xf86-video-amdgpu libva-mesa-driver libva-vdpau-driver
	$(SUDO) $(LINK) $(PWD)/amd/20-amdgpu.conf /etc/X11/xorg.conf.d/20-amdgpu.conf
	$(SUDO) $(LINK) $(PWD)/amd/amdgpu.conf /etc/modprobe.d/amdgpu.conf
	$(SUDO) sed -i 's/^MODULES=(/&amdgpu /' /etc/mkinitcpio.conf

autorandr: $(XDG_CONFIG_HOME) ## Install screen manager
	$(INSTALL) $@
	$(LINK) $(PWD)/$@ $<

backup: ## Backup arch linux packages
	$(MAKEDIR) $(PWD)/archlinux
	$(INSTALLER) -Qnq > $(PWD)/archlinux/abs
	$(INSTALLER) -Qqem > $(PWD)/archlinux/aur

bin: ## Install all binaries
	[ -d $(HOME)/.local/$@ ] && rm -rf $(HOME)/.local/$@ || > /dev/null
	$(LINK) $(PWD)/bin $(HOME)/.local/$@

deploy: ## Deploy all dotfiles
	$(LINK) $(PWD)/zsh/aliases $(HOME)/.config/shell/aliases

dunst: $(XDG_CONFIG_HOME)/dunst ## Install notification service
	$(INSTALL) $@
	$(LINK) $(PWD)/$@/dunstrc $</dunstrc

dwm-git: $(HOME)/.local/src base-devel ## Install dwm-git from AUR with custom patches
	[ -d $</$@ ] && rm -rfv $</$@ || > /dev/null
	$(CLONE) $(AUR_URL)/$@.git $</$@
	$(CLONE) https://gitlab.com/vonbloom/dwm-patches.git $</$@/patches
	cd $</$@/patches && make clean fetch && cp -f PKGBUILD $</$@/
	cd $</$@; makepkg -sic

fontconfig: $(XDG_CONFIG_HOME)/fontconfig ## Setup font configuraion
	echo "ff"
	$(LINK) $(PWD)/$@/fonts.conf $</fonts.conf

gtk: $(XDG_CONFIG_HOME)/gtk-3.0 ## Setup gtk settings
	$(LINK) $(PWD)/gtk-3.0/settings.ini $</settings.ini

opendoas: ## Install doas and configuration
	$(INSTALL) $@
	echo 'permit :wheel' | $(SUDO) tee /etc/doas.conf

maim: sxhkd ## Install screenshooter
	$(INSTALL) maim

strongswan:
	$(INSTALL) $@
	$(ENABLE) $@

sxhkd: $(XDG_CONFIG_HOME)/sxhkd ## Install Simple X Hotkey Deamon and keybindings
	$(INSTALL) $@
	$(LINK) $(PWD)/$@/sxhkdrc $</sxhkdrc

networkmanager: strongswan ## Install NetworkManager and its configuration
	$(INSTALL) $@ $@-strongswan
	for file in $(PWD)/$@/*.nmconnection; do \
		$(SUDO) cp $$file /etc/NetworkManager/system-connections; done
	$(ENABLE) NetworkManager

paru: $(HOME)/.local/src base-devel ## Install paru from AUR
	[ -d $</$@ ] && rm -rf $</$@ || > /dev/null
	$(CLONE) $(AUR_URL)/$@.git $</$@
	cd $</$@; makepkg -sic

redshift: $(XDG_CONFIG_HOME)/redshift ## Install screen color temperature manager
	$(INSTALL) $@
	$(LINK) $(PWD)/$@/$@.conf $</$@.conf

st-git: $(HOME)/.local/src base-devel ## Install st-git from AUR with custom patches
	[ -d $</$@ ] && rm -rf $</$@ || > /dev/null
	$(CLONE) $(AUR_URL)/$@.git $</$@
	$(CLONE) https://gitlab.com/vonbloom/st-patches.git $</$@/patches
	cd $</$@/patches && make fetch && cp -f PKGBUILD $</$@/
	cd $</$@; makepkg -sic

sudo: # Sudo install sudo :)
	$(INSTALLER) -S sudo && exit
	usermod -a -G wheel roger

vim: $(XDG_CONFIG_HOME) ## Install the mighty editor
	$(INSTALL) gvim
	$(LINK) $(PWD)/$@ $<

## Setup the neo mighty editor
neovim: $(XDG_CONFIG_HOME) 
	$(INSTALL) neovim
	$(LINK) $(PWD)/$@ $<

zsh: $(XDG_DATA_HOME)/zsh/plugins $(XDG_CACHE_HOME)/zsh $(XDG_CONFIG_HOME)/zsh neofetch ## Install zsh and its configuration
	$(INSTALL) $@ $@-autosuggestions $@-history-substring-search $@-syntax-highlighting
	[ -d $(HOME)/.local/share/$@/plugins/$@-git-prompt ] && rm -rf $(HOME)/.local/share/$@/plugins/$@-git-prompt || > /dev/null
	$(CLONE) https://github.com/$@-git-prompt/$@-git-prompt.git $(HOME)/.local/share/$@/plugins/$@-git-prompt
	$(MAKEDIR) $(HOME)/.cache/$@
	$(MAKEDIR) $(HOME)/.config/shell
	$(LINK) $(PWD)/$@/.cache/$@/history $(HOME)/.cache/$@/history
	$(LINK) $(PWD)/$@/.config/shell/aliases $(HOME)/.config/shell/aliases
	$(LINK) $(PWD)/$@/.config/$@/.zshenv $(HOME)/.config/$@/.zshenv
	$(LINK) $(PWD)/$@/.config/$@/.zshrc $(HOME)/.config/$@/.zshrc
	$(LINK) $(PWD)/$@/.config/.zprofile $(HOME)/.config/$@/.zprofile
	echo "ZDOTDIR=$(HOME)/.config/zsh" | $(SUDO) tee -a /etc/zsh/zshenv
	chsh -s /bin/$@

wallpapers: $(XDG_DATA_HOME)/wallpapers ## Setup wallpapers
	/bin/rm -f $</*
	$(LINK) $(PWD)/$@/* $</

waybar: $(XDG_CONFIG_HOME)/waybar
	$(INSTALL) $@
	$(LINK) $(PWD)/$@/* $</

base-devel:
	$(INSTALL) $@

$(HOME)%:
	$(MAKEDIR) $@

$(XDG_CONFIG_HOME):
	$(MAKEDIR) $(HOME)/.config

$(XDG_CONFIG_HOME)%: $(XDG_CONFIG_HOME)
	$(MAKEDIR) $@

$(XDG_DATA_HOME):
	$(MAKEDIR) $(HOME)/.local/share

$(XDG_DATA_HOME)%: $(XDG_DATA_HOME)
	$(MAKEDIR) $@

$(XDG_CACHE_HOME):
	$(MAKEDIR) $(HOME)/.cache

$(XDG_CACHE_HOME)%: $(XDG_CACHE_HOME)
	$(MAKEDIR) $@

.PHONY: acpid amdgpu autorandr bin dunst fontconfig maim vim redshift sudo sxhkd zsh

